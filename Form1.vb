﻿Imports System.Text
Imports System.IO

' Version 2.0: Fix bugs and add Home button
Public Class TrackASat

    Private mDome As ASCOM.DriverAccess.Dome
    Private mSelectedDome As String = ""
    Private mTelescope As ASCOM.DriverAccess.Telescope
    Private mSelectedTelescope As String = ""



    Dim CW As Boolean
    Dim CCW As Boolean
    Dim Loaded As Boolean

    Dim lines() As String
    Dim UTCTime() As String
    Dim Az() As String
    Dim Alt() As String

    Private Sub ScopeDome_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        btnDisconnect.Enabled = False
        btnCW.Enabled = False
        btnCCW.Enabled = False
        btnStop.Enabled = False
        cbTrack.Enabled = False
        btnPark.Enabled = False
        btnSetPark.Enabled = False
        btnHome.Enabled = False
        btnLoad.Enabled = False
        cbLock.Enabled = False
        btnTelStop.Enabled = False
        btnOpenShutter.Enabled = False
        btnCloseShutter.Enabled = False
        tbDome.Enabled = True
        tbDomeAz.Enabled = False
        tbShutterState.Enabled = False
        tbTelAlt.Enabled = False
        tbTelAz.Enabled = False
        tbTelescope.Enabled = True
        tbTime.Enabled = False
        Label6.Hide()
        Label7.Hide()
        Label8.Hide()
        Label9.Hide()
        Label10.Hide()
        Label11.Hide()
        Label12.Hide()
        Label13.Hide()
        Label14.Hide()
        Label15.Hide()
        Label16.Hide()

    End Sub

    Private Sub BtnChooseHardware_Click(sender As Object, e As EventArgs) Handles btnChooseHardware.Click
        Try
            mSelectedDome = ASCOM.DriverAccess.Dome.Choose("ASCOM.Simulator.Dome")
            mSelectedTelescope = ASCOM.DriverAccess.Telescope.Choose("ASCOM.Simulator.Telescope")

            If mSelectedDome = "" Then
                MsgBox("Dome not selected")
                Exit Sub
            Else
                tbDome.Text = mSelectedDome
            End If

            If mSelectedTelescope = "" Then
                MsgBox("Telescope not selected")
                Exit Sub
            Else
                tbTelescope.Text = mSelectedTelescope
            End If
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click
        Try
            If mSelectedDome = "" Then
                MsgBox("Dome not Selected")
                Exit Sub
            End If
            If mSelectedTelescope = "" Then
                MsgBox("Telescope not Selected")
                Exit Sub
            End If

            mDome = New ASCOM.DriverAccess.Dome(mSelectedDome)
            mDome.Connected = True
            mTelescope = New ASCOM.DriverAccess.Telescope(mSelectedTelescope)
            mTelescope.Connected = True
            Label8.Show()
            Label9.Show()
            Label10.Show()
            Label11.Show()
            Label12.Show()
            Label13.Show()
            Label14.Show()
            Label15.Show()
            Label16.Show()
            Label6.Show()
            Label7.Show()
            Timer1.Start()
            btnChooseHardware.Enabled = False
            btnConnect.Enabled = False
            btnDisconnect.Enabled = True
            btnCW.Enabled = True
            btnCCW.Enabled = True
            btnStop.Enabled = True
            cbTrack.Enabled = True
            btnPark.Enabled = True
            btnSetPark.Enabled = True
            btnHome.Enabled = True
            btnLoad.Enabled = True
            cbLock.Enabled = True
            btnTelStop.Enabled = True
            btnOpenShutter.Enabled = True
            btnCloseShutter.Enabled = True
            tbDome.Enabled = True
            tbDomeAz.Enabled = True
            tbShutterState.Enabled = True
            tbTelAlt.Enabled = True
            tbTelAz.Enabled = True
            tbTelescope.Enabled = True
            tbTime.Enabled = True
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnDisconnect_Click(sender As Object, e As EventArgs) Handles btnDisconnect.Click
        Try
            If mDome.Connected = True And mTelescope.Connected = True Then
                Label8.Hide()
                Label6.Hide()
                Label7.Hide()
                Label9.Hide()
                Label10.Hide()
                Label11.Hide()
                Label12.Hide()
                Label13.Hide()
                Label14.Hide()
                Label15.Hide()
                Label16.Hide()
                mDome.Connected = False
                mTelescope.Connected = False
                btnChooseHardware.Enabled = True
                btnDisconnect.Enabled = False
                btnCW.Enabled = False
                btnCCW.Enabled = False
                btnStop.Enabled = False
                cbTrack.Enabled = False
                btnPark.Enabled = False
                btnSetPark.Enabled = False
                btnHome.Enabled = False
                btnLoad.Enabled = False
                cbLock.Enabled = False
                btnTelStop.Enabled = False
                btnOpenShutter.Enabled = False
                btnCloseShutter.Enabled = False
                tbDome.Enabled = False
                tbDomeAz.Enabled = False
                tbShutterState.Enabled = False
                tbTelAlt.Enabled = False
                tbTelAz.Enabled = False
                tbTelescope.Enabled = False
                tbTime.Enabled = False
                btnConnect.Enabled = True
            End If
            Label8.Hide()
            Label6.Hide()
            Label7.Hide()
            Label9.Hide()
            Label10.Hide()
            Label11.Hide()
            Label12.Hide()
            Label13.Hide()
            Label14.Hide()
            Label15.Hide()
            Label16.Hide()
            btnConnect.Enabled = True
            btnCW.Enabled = False
            btnCCW.Enabled = False
            btnStop.Enabled = False
            cbTrack.Enabled = False
            btnPark.Enabled = False
            btnSetPark.Enabled = False
            btnHome.Enabled = False
            btnLoad.Enabled = False
            cbLock.Enabled = False
            btnTelStop.Enabled = False
            btnOpenShutter.Enabled = False
            btnCloseShutter.Enabled = False
            tbDome.Enabled = False
            tbDomeAz.Enabled = False
            tbShutterState.Enabled = False
            tbTelAlt.Enabled = False
            tbTelAz.Enabled = False
            tbTelescope.Enabled = False
            tbTime.Enabled = False
        Catch ex As Exception
            'MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnOpenShutter_Click(sender As Object, e As EventArgs) Handles btnOpenShutter.Click
        Try
            'btnOpenShutter.Enabled = False
            'btnCloseShutter.Enabled = True
            mDome.OpenShutter()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnCloseShutter_Click(sender As Object, e As EventArgs) Handles btnCloseShutter.Click
        Try
            'btnCloseShutter.Enabled = False
            'btnOpenShutter.Enabled = True
            mDome.CloseShutter()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnPark_Click(sender As Object, e As EventArgs) Handles btnPark.Click
        Try
            cbTrack.Checked = False
            mDome.AbortSlew()
            mDome.Park()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnSetPark_Click(sender As Object, e As EventArgs) Handles btnSetPark.Click
        Try
            mDome.SetPark()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        Try
            'cbLock.Checked = False Stop button for telescope
            cbTrack.Checked = False
            CW = False
            CCW = False
            mDome.AbortSlew()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub CbTrack_CheckedChanged(sender As Object, e As EventArgs) Handles cbTrack.CheckedChanged

    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Try
            tbTelAz.Text = mTelescope.Azimuth.ToString("n6")
            tbTelAlt.Text = mTelescope.Altitude.ToString("n6")
            tbDomeAz.Text = mDome.Azimuth.ToString("n6")
            Dim timeNow As String = DateTime.UtcNow.ToUniversalTime.ToString("yyyyMMddhhmmss")
            tbTime.Text = timeNow

            If CW = True Then
                Dim currAz As Double = mDome.Azimuth
                Dim targetAz As Double = currAz + 50 ' step to move
                Dim diffAz As Double = Math.Abs(currAz - targetAz)
                If targetAz > 360 Then
                    targetAz = targetAz - 360
                ElseIf targetAz < 0 Then
                    targetAz = targetAz + 360
                End If
                mDome.SlewToAzimuth(targetAz)
            End If

            If CCW = True Then
                Dim currAz As Double = mDome.Azimuth
                Dim targetAz As Double = currAz - 50 ' step to move
                Dim diffAz As Double = Math.Abs(currAz - targetAz)
                If targetAz > 360 Then
                    targetAz = targetAz - 360
                ElseIf targetAz < 0 Then
                    targetAz = targetAz + 360
                End If
                mDome.SlewToAzimuth(targetAz)
            End If

            If mDome.ShutterStatus = 1 Then
                tbShutterState.Text = "Idle"
            ElseIf mDome.ShutterStatus = 2 Then
                tbShutterState.Text = "Opening"
            ElseIf mDome.ShutterStatus = 3 Then
                tbShutterState.Text = "Closing"
            End If

            If cbLock.Checked = True Then
                Dim ind As Int32
                ind = UTCTime.ToList().IndexOf(timeNow)
                hour.Text = ind
                If ind <> -1 Then
                    Dim Azimuth As Double = CDbl(Az(ind))
                    Dim Altitude As Double = CDbl(Alt(ind))
                    mTelescope.SlewToAltAz(Azimuth, Altitude)
                End If
                Loaded = False
            End If

            If cbTrack.Checked = True Then
                Dim diff As Decimal = Math.Abs(mTelescope.Azimuth - mDome.Azimuth)
                If diff > 2 Then
                    mDome.SlewToAzimuth(mTelescope.Azimuth)
                Else
                    mDome.AbortSlew()
                End If
            End If
        Catch ex As Exception
            'MsgBox("Error :" & ex.Message)
        End Try
    End Sub



    Private Sub BtnLoad_Click(sender As Object, e As EventArgs) Handles btnLoad.Click
        Dim path As String = "C:\Users\Mikhael\Desktop\DomeThailandVB_ScopeDome_2.0\TimeAzAlt.txt"
        lines = File.ReadAllLines(path)
        ReDim UTCTime(lines.Length - 1)
        ReDim Az(lines.Length - 1)
        ReDim Alt(lines.Length - 1)
        Dim s As String
        Dim count As Integer = 0
        For Each s In lines
            Dim sep() As String = Split(lines(0), ",")
            If sep.Length = 3 Then
                Dim seperated() As String = Split(s, ",")
                UTCTime(count) = seperated(0)
                Az(count) = seperated(1)
                Alt(count) = seperated(2)
                Loaded = True
                'MsgBox("Time, Azimuth, Altitude loaded successfully.")
            Else
                Loaded = True
                MsgBox("TimeAzAlt does not follow convention OR No file loaded.")
                Exit Sub
            End If
            count = count + 1
        Next s
        MsgBox("Time, Azimuth, Altitude loaded successfully.")
    End Sub

    Private Sub CbLock_CheckedChanged(sender As Object, e As EventArgs) Handles cbLock.CheckedChanged
        If Loaded = True Then
            cbLock.Checked = True
        Else
            cbLock.Checked = False
            Loaded = False
        End If
    End Sub

    Private Sub BtnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Try
            cbLock.Checked = False
            cbTrack.Checked = False
            mDome.FindHome()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try

    End Sub

    Private Sub BtnTelStop_Click(sender As Object, e As EventArgs) Handles btnTelStop.Click
        Try
            cbLock.Checked = False
            mTelescope.AbortSlew()

        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnTelHome_Click(sender As Object, e As EventArgs) Handles btnTelHome.Click
        Try
            cbLock.Checked = False
            cbTrack.Checked = False
            mTelescope.AbortSlew()
            mTelescope.Park()
        Catch ex As Exception
            MsgBox("Error: " & ex.Message)
        End Try
    End Sub

    Private Sub BtnCW_Click(sender As Object, e As EventArgs) Handles btnCW.Click
        CW = True
        CCW = False
    End Sub

    Private Sub BtnCCW_Click(sender As Object, e As EventArgs) Handles btnCCW.Click
        CCW = True
        CW = False
    End Sub
End Class
