﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class TrackASat
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnChooseHardware = New System.Windows.Forms.Button()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.tbDome = New System.Windows.Forms.TextBox()
        Me.tbTelescope = New System.Windows.Forms.TextBox()
        Me.btnDisconnect = New System.Windows.Forms.Button()
        Me.tbDomeAz = New System.Windows.Forms.TextBox()
        Me.tbTelAz = New System.Windows.Forms.TextBox()
        Me.tbTelAlt = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnCW = New System.Windows.Forms.Button()
        Me.btnCCW = New System.Windows.Forms.Button()
        Me.cbTrack = New System.Windows.Forms.CheckBox()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnHome = New System.Windows.Forms.Button()
        Me.btnSetPark = New System.Windows.Forms.Button()
        Me.btnPark = New System.Windows.Forms.Button()
        Me.btnCloseShutter = New System.Windows.Forms.Button()
        Me.btnOpenShutter = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnTelHome = New System.Windows.Forms.Button()
        Me.btnTelStop = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnLoad = New System.Windows.Forms.Button()
        Me.cbLock = New System.Windows.Forms.CheckBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.tbShutterState = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.tbTime = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.minute = New System.Windows.Forms.TextBox()
        Me.second = New System.Windows.Forms.TextBox()
        Me.hour = New System.Windows.Forms.TextBox()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnChooseHardware
        '
        Me.btnChooseHardware.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnChooseHardware.Location = New System.Drawing.Point(12, 12)
        Me.btnChooseHardware.Name = "btnChooseHardware"
        Me.btnChooseHardware.Size = New System.Drawing.Size(75, 23)
        Me.btnChooseHardware.TabIndex = 0
        Me.btnChooseHardware.Text = "Hardware"
        Me.btnChooseHardware.UseVisualStyleBackColor = True
        '
        'btnConnect
        '
        Me.btnConnect.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnConnect.Location = New System.Drawing.Point(12, 41)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(75, 23)
        Me.btnConnect.TabIndex = 1
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'tbDome
        '
        Me.tbDome.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbDome.Location = New System.Drawing.Point(93, 12)
        Me.tbDome.Name = "tbDome"
        Me.tbDome.ReadOnly = True
        Me.tbDome.Size = New System.Drawing.Size(100, 20)
        Me.tbDome.TabIndex = 2
        Me.tbDome.Text = "---Dome---"
        Me.tbDome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbTelescope
        '
        Me.tbTelescope.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbTelescope.Location = New System.Drawing.Point(199, 12)
        Me.tbTelescope.Name = "tbTelescope"
        Me.tbTelescope.ReadOnly = True
        Me.tbTelescope.Size = New System.Drawing.Size(100, 20)
        Me.tbTelescope.TabIndex = 3
        Me.tbTelescope.Text = "---Telescope---"
        Me.tbTelescope.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnDisconnect
        '
        Me.btnDisconnect.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnDisconnect.Location = New System.Drawing.Point(12, 70)
        Me.btnDisconnect.Name = "btnDisconnect"
        Me.btnDisconnect.Size = New System.Drawing.Size(75, 23)
        Me.btnDisconnect.TabIndex = 4
        Me.btnDisconnect.Text = "Disconnect"
        Me.btnDisconnect.UseVisualStyleBackColor = True
        '
        'tbDomeAz
        '
        Me.tbDomeAz.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbDomeAz.Location = New System.Drawing.Point(39, 26)
        Me.tbDomeAz.Name = "tbDomeAz"
        Me.tbDomeAz.Size = New System.Drawing.Size(90, 20)
        Me.tbDomeAz.TabIndex = 5
        Me.tbDomeAz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbTelAz
        '
        Me.tbTelAz.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbTelAz.Location = New System.Drawing.Point(39, 26)
        Me.tbTelAz.Name = "tbTelAz"
        Me.tbTelAz.Size = New System.Drawing.Size(90, 20)
        Me.tbTelAz.TabIndex = 6
        Me.tbTelAz.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tbTelAlt
        '
        Me.tbTelAlt.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbTelAlt.Location = New System.Drawing.Point(39, 52)
        Me.tbTelAlt.Name = "tbTelAlt"
        Me.tbTelAlt.Size = New System.Drawing.Size(90, 20)
        Me.tbTelAlt.TabIndex = 7
        Me.tbTelAlt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnCW)
        Me.GroupBox1.Controls.Add(Me.btnCCW)
        Me.GroupBox1.Controls.Add(Me.cbTrack)
        Me.GroupBox1.Controls.Add(Me.btnStop)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.tbDomeAz)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.Info
        Me.GroupBox1.Location = New System.Drawing.Point(12, 102)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(135, 231)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Dome"
        '
        'btnCW
        '
        Me.btnCW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCW.Location = New System.Drawing.Point(69, 95)
        Me.btnCW.Name = "btnCW"
        Me.btnCW.Size = New System.Drawing.Size(45, 45)
        Me.btnCW.TabIndex = 17
        Me.btnCW.Text = "CW"
        Me.btnCW.UseVisualStyleBackColor = True
        '
        'btnCCW
        '
        Me.btnCCW.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCCW.Location = New System.Drawing.Point(18, 95)
        Me.btnCCW.Name = "btnCCW"
        Me.btnCCW.Size = New System.Drawing.Size(45, 45)
        Me.btnCCW.TabIndex = 16
        Me.btnCCW.Text = "CCW"
        Me.btnCCW.UseVisualStyleBackColor = True
        '
        'cbTrack
        '
        Me.cbTrack.AutoSize = True
        Me.cbTrack.Location = New System.Drawing.Point(9, 64)
        Me.cbTrack.Name = "cbTrack"
        Me.cbTrack.Size = New System.Drawing.Size(54, 17)
        Me.cbTrack.TabIndex = 12
        Me.cbTrack.Text = "Track"
        Me.cbTrack.UseVisualStyleBackColor = True
        '
        'btnStop
        '
        Me.btnStop.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnStop.Location = New System.Drawing.Point(69, 60)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(60, 23)
        Me.btnStop.TabIndex = 13
        Me.btnStop.Text = "Stop"
        Me.btnStop.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Az (°)"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnHome)
        Me.Panel1.Controls.Add(Me.btnSetPark)
        Me.Panel1.Controls.Add(Me.btnPark)
        Me.Panel1.Location = New System.Drawing.Point(6, 155)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(123, 68)
        Me.Panel1.TabIndex = 23
        '
        'btnHome
        '
        Me.btnHome.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnHome.Location = New System.Drawing.Point(30, 37)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(60, 23)
        Me.btnHome.TabIndex = 24
        Me.btnHome.Text = "Home"
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnSetPark
        '
        Me.btnSetPark.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnSetPark.Location = New System.Drawing.Point(58, 8)
        Me.btnSetPark.Name = "btnSetPark"
        Me.btnSetPark.Size = New System.Drawing.Size(60, 23)
        Me.btnSetPark.TabIndex = 12
        Me.btnSetPark.Text = "Set"
        Me.btnSetPark.UseVisualStyleBackColor = True
        '
        'btnPark
        '
        Me.btnPark.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnPark.Location = New System.Drawing.Point(2, 8)
        Me.btnPark.Name = "btnPark"
        Me.btnPark.Size = New System.Drawing.Size(60, 23)
        Me.btnPark.TabIndex = 11
        Me.btnPark.Text = "Park"
        Me.btnPark.UseVisualStyleBackColor = True
        '
        'btnCloseShutter
        '
        Me.btnCloseShutter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnCloseShutter.Location = New System.Drawing.Point(69, 54)
        Me.btnCloseShutter.Name = "btnCloseShutter"
        Me.btnCloseShutter.Size = New System.Drawing.Size(60, 23)
        Me.btnCloseShutter.TabIndex = 11
        Me.btnCloseShutter.Text = "Close"
        Me.btnCloseShutter.UseVisualStyleBackColor = True
        '
        'btnOpenShutter
        '
        Me.btnOpenShutter.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnOpenShutter.Location = New System.Drawing.Point(9, 54)
        Me.btnOpenShutter.Name = "btnOpenShutter"
        Me.btnOpenShutter.Size = New System.Drawing.Size(60, 23)
        Me.btnOpenShutter.TabIndex = 10
        Me.btnOpenShutter.Text = "Open"
        Me.btnOpenShutter.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnTelHome)
        Me.GroupBox2.Controls.Add(Me.btnTelStop)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Controls.Add(Me.tbTelAz)
        Me.GroupBox2.Controls.Add(Me.tbTelAlt)
        Me.GroupBox2.Controls.Add(Me.btnLoad)
        Me.GroupBox2.Controls.Add(Me.cbLock)
        Me.GroupBox2.ForeColor = System.Drawing.SystemColors.Info
        Me.GroupBox2.Location = New System.Drawing.Point(164, 102)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(135, 140)
        Me.GroupBox2.TabIndex = 9
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Telescope"
        '
        'btnTelHome
        '
        Me.btnTelHome.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnTelHome.Location = New System.Drawing.Point(9, 107)
        Me.btnTelHome.Name = "btnTelHome"
        Me.btnTelHome.Size = New System.Drawing.Size(60, 23)
        Me.btnTelHome.TabIndex = 24
        Me.btnTelHome.Text = "Home"
        Me.btnTelHome.UseVisualStyleBackColor = True
        '
        'btnTelStop
        '
        Me.btnTelStop.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnTelStop.Location = New System.Drawing.Point(69, 107)
        Me.btnTelStop.Name = "btnTelStop"
        Me.btnTelStop.Size = New System.Drawing.Size(60, 23)
        Me.btnTelStop.TabIndex = 23
        Me.btnTelStop.Text = "Stop"
        Me.btnTelStop.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "Alt (°)"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(6, 29)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Az (°)"
        '
        'btnLoad
        '
        Me.btnLoad.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnLoad.Location = New System.Drawing.Point(9, 78)
        Me.btnLoad.Name = "btnLoad"
        Me.btnLoad.Size = New System.Drawing.Size(60, 23)
        Me.btnLoad.TabIndex = 18
        Me.btnLoad.Text = "Load"
        Me.btnLoad.UseVisualStyleBackColor = True
        '
        'cbLock
        '
        Me.cbLock.AutoSize = True
        Me.cbLock.Location = New System.Drawing.Point(82, 82)
        Me.cbLock.Name = "cbLock"
        Me.cbLock.Size = New System.Drawing.Size(50, 17)
        Me.cbLock.TabIndex = 23
        Me.cbLock.Text = "Lock"
        Me.cbLock.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 200
        '
        'tbShutterState
        '
        Me.tbShutterState.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbShutterState.Location = New System.Drawing.Point(52, 28)
        Me.tbShutterState.Name = "tbShutterState"
        Me.tbShutterState.Size = New System.Drawing.Size(76, 20)
        Me.tbShutterState.TabIndex = 10
        Me.tbShutterState.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(5, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(32, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "State"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.tbShutterState)
        Me.GroupBox3.Controls.Add(Me.btnCloseShutter)
        Me.GroupBox3.Controls.Add(Me.btnOpenShutter)
        Me.GroupBox3.ForeColor = System.Drawing.SystemColors.Info
        Me.GroupBox3.Location = New System.Drawing.Point(164, 248)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(135, 85)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Shutter"
        '
        'tbTime
        '
        Me.tbTime.ForeColor = System.Drawing.SystemColors.ControlText
        Me.tbTime.Location = New System.Drawing.Point(93, 72)
        Me.tbTime.Name = "tbTime"
        Me.tbTime.Size = New System.Drawing.Size(100, 20)
        Me.tbTime.TabIndex = 16
        Me.tbTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.Info
        Me.Label5.Location = New System.Drawing.Point(111, 57)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(61, 13)
        Me.Label5.TabIndex = 17
        Me.Label5.Text = "Time (UTC)"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Corbel", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.Control
        Me.Label6.Location = New System.Drawing.Point(199, 37)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(100, 33)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Track A"
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Corbel", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.Control
        Me.Label7.Location = New System.Drawing.Point(201, 66)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(95, 33)
        Me.Label7.TabIndex = 24
        Me.Label7.Text = "Sat 2.1"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label8.Location = New System.Drawing.Point(88, 28)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(18, 26)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "."
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label9.Location = New System.Drawing.Point(100, 28)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(18, 26)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "."
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label10.Location = New System.Drawing.Point(112, 28)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(18, 26)
        Me.Label10.TabIndex = 27
        Me.Label10.Text = "."
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label11.Location = New System.Drawing.Point(124, 28)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(18, 26)
        Me.Label11.TabIndex = 28
        Me.Label11.Text = "."
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label12.Location = New System.Drawing.Point(136, 28)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(18, 26)
        Me.Label12.TabIndex = 29
        Me.Label12.Text = "."
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label13.Location = New System.Drawing.Point(148, 28)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(18, 26)
        Me.Label13.TabIndex = 30
        Me.Label13.Text = "."
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label14.Location = New System.Drawing.Point(160, 28)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(18, 26)
        Me.Label14.TabIndex = 31
        Me.Label14.Text = "."
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label15.Location = New System.Drawing.Point(172, 28)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(18, 26)
        Me.Label15.TabIndex = 32
        Me.Label15.Text = "."
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Corbel", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label16.Location = New System.Drawing.Point(184, 28)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(18, 26)
        Me.Label16.TabIndex = 33
        Me.Label16.Text = "."
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'minute
        '
        Me.minute.Location = New System.Drawing.Point(402, 66)
        Me.minute.Name = "minute"
        Me.minute.Size = New System.Drawing.Size(100, 20)
        Me.minute.TabIndex = 21
        '
        'second
        '
        Me.second.Location = New System.Drawing.Point(402, 92)
        Me.second.Name = "second"
        Me.second.Size = New System.Drawing.Size(100, 20)
        Me.second.TabIndex = 22
        '
        'hour
        '
        Me.hour.Location = New System.Drawing.Point(402, 40)
        Me.hour.Name = "hour"
        Me.hour.Size = New System.Drawing.Size(100, 20)
        Me.hour.TabIndex = 20
        '
        'TrackASat
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Desktop
        Me.ClientSize = New System.Drawing.Size(311, 347)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.second)
        Me.Controls.Add(Me.minute)
        Me.Controls.Add(Me.hour)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tbTime)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnDisconnect)
        Me.Controls.Add(Me.tbTelescope)
        Me.Controls.Add(Me.tbDome)
        Me.Controls.Add(Me.btnConnect)
        Me.Controls.Add(Me.btnChooseHardware)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Name = "TrackASat"
        Me.Text = "Track A Sat 2.0"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnChooseHardware As Button
    Friend WithEvents btnConnect As Button
    Friend WithEvents tbDome As TextBox
    Friend WithEvents tbTelescope As TextBox
    Friend WithEvents btnDisconnect As Button
    Friend WithEvents tbDomeAz As TextBox
    Friend WithEvents tbTelAz As TextBox
    Friend WithEvents tbTelAlt As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents btnOpenShutter As Button
    Friend WithEvents cbTrack As CheckBox
    Friend WithEvents btnStop As Button
    Friend WithEvents btnSetPark As Button
    Friend WithEvents btnPark As Button
    Friend WithEvents btnCloseShutter As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Label4 As Label
    Friend WithEvents tbShutterState As TextBox
    Friend WithEvents btnCW As Button
    Friend WithEvents btnCCW As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents tbTime As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents btnLoad As Button
    Friend WithEvents cbLock As CheckBox
    Friend WithEvents btnHome As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnTelStop As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents btnTelHome As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents minute As TextBox
    Friend WithEvents second As TextBox
    Friend WithEvents hour As TextBox
End Class
